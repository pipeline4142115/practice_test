#!/bin/bash
 
# GitHub Repository URL
github_repo="https://gitlab.com/pipeline4142115/repovalidation.git"
 
# GitLab Repository URL
gitlab_repo="https://github.com/sandeeptekkam67/repovalidation.git"
 
# Function to list branches
list_branches() {
    git ls-remote --heads $1 | awk '{print $2}' | sed 's/refs\/heads\///' | sort
}
 
# Function to get commit IDs
get_commit_id() {
    git ls-remote $1 $2 | awk '{print $1}'
}
 
# Function to get commit history
get_commit_history() {
    git log --pretty=format:"%H" --reverse $1
}
 
# Function to compare branches
compare_branches() {
    gitlab_branches=$(list_branches $gitlab_repo)
    github_branches=$(list_branches $github_repo)
 
    echo "GitLab Branch          | GitHub Branch         | GitLab Commit               | GitHub Commit              | Status"
    echo "----------------------------------------------------------------------------------------------------------------"
 
    for gitlab_branch in $gitlab_branches; do
        github_branch=$(echo "$github_branches" | grep "$gitlab_branch")
        gitlab_commit=$(get_commit_id $gitlab_repo $gitlab_branch)
        github_commit=$(get_commit_id $github_repo $github_branch)
 
        if [ -z "$github_branch" ]; then
            github_branch="No branch"
            github_commit="No commit"
        fi
 
        if [ -z "$gitlab_branch" ]; then
            gitlab_branch="No branch"
            gitlab_commit="No commit"
        fi
 
        status="Not matched"
        if [ "$gitlab_commit" == "$github_commit" ]; then
            status="Matched"
        fi
 
        printf "%-24s | %-24s | %-28s | %-28s | %-10s\n" "$gitlab_branch" "$github_branch" "$gitlab_commit" "$github_commit" "$status"
 
        # Print commit history if branch not available in both repositories
        if [ "$github_branch" == "No branch" ]; then
            echo "Commit history for GitLab Branch: $gitlab_branch"
            get_commit_history $gitlab_branch
            echo
        fi
    done
}
 
# Main
compare_branches