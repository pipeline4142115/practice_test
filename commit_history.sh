#!/bin/bash
 
# GitHub Repository URL
github_repo="https://gitlab.com/pipeline4142115/repovalidation.git"
 
# GitLab Repository URL
gitlab_repo="https://github.com/sandeeptekkam67/repovalidation.git"
 
# Function to list branches
list_branches() {
    git ls-remote --heads $1 | awk '{print $2}' | sed 's/refs\/heads\///' | sort
}
 
# Function to compare branches
compare_branches() {
    gitlab_branches=$(list_branches $gitlab_repo)
    github_branches=$(list_branches $github_repo)
 
    gitlab_count=$(echo "$gitlab_branches" | wc -l)
    github_count=$(echo "$github_branches" | wc -l)
    max_count=$((gitlab_count > github_count ? gitlab_count : github_count))
 
    echo "GitLab Branches                GitHub Branches"
 
    for ((i=1; i<=$max_count; i++)); do
        gitlab_branch=$(echo "$gitlab_branches" | sed -n "${i}p")
        github_branch=$(echo "$github_branches" | sed -n "${i}p")
 
        printf "%-30s %-30s\n" "${gitlab_branch:- --------------}" "${github_branch:- --------------}"
    done
}
 
# Main
compare_branches